import csv
import time
import itertools


def search_improved(s,rows):#apply rankings to school data using search phrase

    s = s.lower() # it is a nice feature to ignore case
    for item in rows: # iterate through the data dicts
        found = False # this variable indicated if the row matches
        rank = 0 # rank of the row
        
        school_name_word_count = 0 # how many words in the school name matches the search (I've assumed school name takes priority)
        
        #Compare every word in the search to every word in the school name and increase rank accordingly
        for s_word in s.lower().split():
            for i_word in item["SCHNAM05"].lower().split():
                if i_word == s_word:
                    found = True
                    rank+=5
                    school_name_word_count += 1
        word_multiplier = school_name_word_count/ len(s.split()) # how close the search string was to the school name increase rank accordingly
        
        if s.lower() == item["SCHNAM05"].lower():
            rank += 99 # increase rank by alot if the school name matches
            found = True
        
        #Compare every word in the search to every word in the city name
        for s_word in s.lower().split():
            for i_word in item["LCITY05"].lower().split():
                if i_word == s_word:
                    found = True
                    rank+= (2+5*word_multiplier)
        if s.lower() == item["LCITY05"].lower():
            rank += 10 # increase rank by moderate amount if the city name matches
            found = True
        
        #Compare every word in the search to every word in the state name
        for s_word in s.lower().split():
            for i_word in item["LSTATE05"].lower().split():
                if i_word == s_word: # if any value contains s
                    found = True
                    rank+= (2+5*word_multiplier)
        if s.lower() == item["LSTATE05"].lower():
            rank += 10 # increase rank by moderate amount if the state name matches
            found = True

        if found:
            item.update({"rank": rank}) # add the rank to the dict
            yield item # generate the item



def search_schools(search):
    #Opening and read the csv file
    with open('school_data.csv', newline='', errors='ignore') as csvfile:
        dict_reader = csv.DictReader(csvfile)
        rows = list(dict_reader)# converting to python list iterable
        total_rows = len(rows) # calculating total rows
   
    search_results = []
    for result in itertools.islice(search_improved(search,rows),total_rows): # iterate and rank data
        search_results.append(result)

    start = time.time()  # start the timer
    search_results.sort(key=lambda x:x['rank']) # sort by rank
    
    #calculate and print time taken
    end = time.time()
    print("Results for \"{}\" (search took: {}s)".format(search,end - start))
    
    for i, result in enumerate(search_results[::-1]):# iterate data by descending ranks
        if i > 2:# if the third result has been shown
            break
        #Print school data
        print("{}. {}".format(i+1,result["SCHNAM05"]))
        print("{}, {}".format(result["LCITY05"],result["LSTATE05"]))
    

search_schools("chicago")