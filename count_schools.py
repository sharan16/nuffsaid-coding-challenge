import csv
import itertools

# Print the counts related to the school_data.csv file
def print_counts():
    
    #Opening and read the csv file
    with open('school_data.csv', newline='',errors='ignore') as csvfile:
        dict_reader = csv.DictReader(csvfile)
        rows = list(dict_reader)#converting to python list iterable
        total_rows = len(rows) #calculating total rows

    #Print the total schools
    print("Total Schools: ", total_rows)
    
    #----------------schools grouped by state-----------------#
    #Print the amount of schools grouped by state
    print("Schools by State:")
    
    for key, group in itertools.groupby(rows, lambda item: item["LSTATE05"]):
        print (key,":", sum(1 for x in group))# Sum the groups
    #---------------------------------------------------------#
    
    
    
    #----------------schools grouped by MLOCALE---------------#
    #Print the amount of schools grouped by MLOCALE
    print("Schools by Metro-centric locale:")
    rows.sort(key=lambda x:x['MLOCALE']) #Sort by MLOCALE    
    
    for key, group in itertools.groupby(rows, lambda item: item["MLOCALE"]):# iterate the data grouped by MLOCALE
        print (key,":",sum(1 for x in group))# Sum the groups
    #---------------------------------------------------------#

    
    
    #---------------School and city data----------------------#
    rows.sort(key=lambda x:x['LCITY05']) #Sort by city
    max_school_count = 0
    max_city = ""
    school_cities = []
    for key, group in itertools.groupby(rows, lambda item: item["LCITY05"]):# iterate the data grouped by CITY
        group_count = sum(1 for x in group) # count the groups
        if group_count >= 1: # if the city has atleast one school
            school_cities.append(key)
        if group_count > max_school_count:# update the city with the max schools
            max_school_count = group_count
            max_city = key
    
    # Print required city data
    print("City with most schools: {} ({} schools)".format(max_city,max_school_count) )
    print("Unique cities with at least one school: {}".format(len(school_cities)))
    #---------------------------------------------------------#

print_counts()